﻿// Goldie: GOLD Engine for D
// GoldieLib
// Written in the D programming language.

module goldie.ver;

import semitwist.util.all;

enum goldieVerStr = "0.9.1";
enum Ver goldieVer = goldieVerStr.toVer();
